# -*- coding: utf-8 -*-
#!/usr/bin/python

# -----------------------------------------------------------------
# imports
# -----------------------------------------------------------------

import os
import SocketServer
import sys
import argparse
from time import time, sleep
import datetime
import random
from Queue import *
import threading
import time
import psycopg2

users = {}
DB = None

def python_to_lua(tmp):
    #value = str(tmp)
    #value = value.replace(":", "=")
    typed_list = {}
    for key, value in tmp.items():
        if type(value) == str:
            typed_list[key] = '"'+value+'"'
        elif type(value) == bool:
            typed_list[key] = str(value).lower()
        elif type(value) == list:
            typed_list[key] = str(value).replace("[", "{").replace("]", "}")
        else:
            typed_list[key] = str(value)
    return "{{{}}}".format(", ".join("{} = {}".format(key, typed_list[key]) for key in typed_list))


class TCPHandler(SocketServer.BaseRequestHandler):

    def handle(self):
        data = self.request.recv(1024).strip()
        #print(data)
        if data.find("Connection test") > -1:
            self.request.send(data.upper())
        else:
            self.request.send(use(data))
        return


class UDPHandler(SocketServer.BaseRequestHandler):

    def handle(self):
        data = self.request[0].strip()
        #print(data)
        socket = self.request[1]
        value = use(data)
        socket.sendto(value, self.client_address)


def updater():
    global DB
    timer = 1
    while True:
        startTime = time.time()
        if DB is not None:
            if DB.timer > 0:
                DB.timer -= timer
            else:
                try:
                    DB.insert_sql("SELECT 1")
                except:
                    if DB.connected:
                        DB.disconnect()
                    DB.connect()
                    DB.connected = True
                DB.timer = DB.timer_base
        endTime = time.time() - startTime
        sleep(timer - endTime)

# -----------------------------------------------------------------
#  Classes
# -----------------------------------------------------------------


class db():

    def __init__(self, address, user, password, db):
        self.address = address
        self.user = user
        self.password = password
        self.db = db
        self.connect_str = ""
        self.connect_str += "dbname='" + self.db + "' "
        self.connect_str += "user='" + self.user + "' "
        self.connect_str += "host='" + self.address + "' "
        self.connect_str += "password='" + self.password + "' "
        self.conn = None
        self.connected = False
        self.timer = 0
        self.timer_base = 30
        return

    def connect(self):
        print("INFO: reconnecting db")
        try:
            self.conn = psycopg2.connect(self.connect_str)
        except:
            print("I am unable to connect to the database")
            print("Host:" + self.address + " Database:" + self.db)
            exit(1)
        self.cur = self.conn.cursor()
        print("INFO: reconnected")

    def disconnect(self):
        self.conn.commit()
        self.cur.close()
        self.conn.close()

    def get_sql(self, sql):
        self.cur.execute(sql)
        rows = self.cur.fetchall()
        self.timer = self.timer_base
        return rows

    def insert_sql(self, sql):
        self.cur.execute(sql)
        self.conn.commit()
        self.timer = self.timer_base


class action():

    def __init__(self, values):
        self.id = int(values[0])
        self.time = values[1]
        self.act = values[2]


class user():

    def __init__(self, values):
        self.id = int(values[0])
        self.name = values[1]
        self.password = values[2]
        self.register = values[3]
        self.action = values[4]

    def add_action(self, values):
        self.action.append(action(values))

    def get(self):
        tmp = {}
        tmp["id"] = self.id
        tmp["name"] = self.name
        tmp["register"] = self.register
        tmp["action"] = self.action
        return python_to_lua(tmp)

# -----------------------------------------------------------------
# Game server commands
# -----------------------------------------------------------------


def register(values):
    sql = "INSERT INTO users.info(name, password, registration) "
    sql += "VALUES("
    sql += "$string$" + values[0] + "$string$,"
    sql += "$string$" + values[1] + "$string$,"
    sql += "now()"
    sql += ");"
    DB.insert_sql(sql)
    return "DONE"


def login(values):
    global users
    sql = """
SELECT id, name,
password, registration::TEXT,
COALESCE(
(SELECT action from users.actions
where "user"=info.id
and time::date = now()::date
order by id desc LIMIT 1),
'Other') as action
FROM users.info WHERE
    """
    sql += " name = '%s'" % values[0]
    sql += " and password = '%s'" % values[1]
    result = DB.get_sql(sql)
    name = None
    for i in result:
        name = str(i[1])
        users[i[1]] = user(i)
    if name is None:
        return "ERROR"
    return users[name].get() + " --"


def add_action(values):
    global users
    sql = "INSERT INTO users.actions(\"user\", action, time) "
    sql += "VALUES("
    sql += values[0] + ","
    sql += "$string$" + values[1] + "$string$,"
    sql += "now()"
    sql += ");"
    try:
        DB.insert_sql(sql)
        users[values[0]].action = values[1]
        return "DONE"
    except:
        return "ERROR"


def list_day(values):
    global users
    if len(values) < 4:
        return "ERROR"
    sql = """SELECT action,
SUM(duration)::TEXT as duration
FROM
(select action, time, "user",
(COALESCE(LAG(i.time)        OVER (ORDER BY i.time DESC),
((CASE WHEN (time::date+'23 hours 59 minutes'::interval) > now() then now()
else (time::date+'23 hours 59 minutes'::interval) end)
)))-time AS duration
from users.actions i
order by time) as a
"""
    sql += " WHERE \"user\" = " + values[0]
    sql += " AND EXTRACT(year FROM \"time\") = " + values[1]
    sql += " AND EXTRACT(month FROM \"time\") = " + values[2]
    sql += " AND EXTRACT(day FROM \"time\") = " + values[3]
    sql += " group by action;"
    result = DB.get_sql(sql)
    return_string = "{ "
    for i in result:
        tmp = {}
        tmp["action"] = i[0]
        tmp["duration"] = i[1]
        return_string += python_to_lua(tmp) + ","
    return_string += "} --"
    #TODO safe for later?
    return return_string


# -----------------------------------------------------------------
# General functions
# -----------------------------------------------------------------


def usage():
    tmp = ""
    for i,v in commands.iteritems():
        tmp += "Command: " + i.ljust(20) + "\t Params: " + v["params"].ljust(20) + "\t Description: " +  v["desc"] + "\n"
    return tmp


def use(data):
    if not data:
        return ""
    tmp = data.split()
    command = tmp[0]
    del tmp[0]
    #print(command, tmp)
    if command.lower() == "help":
        return usage()
    try:
        return commands[command.lower()]["command"](tmp)
    except:
        return sys.exc_info()


def set_db(address, user, password, database):
    global DB
    DB = db(address, user, password, database)


def set_commands(data):
    global commands
    #print(data)
    commands = data
