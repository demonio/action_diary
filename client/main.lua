local language = {}
local instance = "register"
user = {}
local love = love
local tostring = tostring
local string = string
local functions = require "openfun-core.functions"
local gameinstance = require 'states'
local default_font = love.graphics.newFont( 30 )
local toggle = false

function love.load()
  love.graphics.setFont(default_font)
end

function love.update(dt)
  if not gameinstance[instance].first then
      gameinstance[instance].instance = instance
      gameinstance[instance].player = player
      if gameinstance[instance].load then gameinstance[instance].load() end
      gameinstance[instance].first = true
  end
  if gameinstance[instance].first and gameinstance[instance].update then
    if gameinstance[instance].updater then
      player = gameinstance[instance].player
      gameinstance[instance].updater = false
    end
    if gameinstance[instance].instance ~= instance  then
       instance = gameinstance[instance].instance
       gameinstance[instance].first = false
    end
    gameinstance[instance].update(dt)
  end
  --love.graphics.setFont(default_font)
  --print(gameinstance[instance].instance)
end

function love.draw()
  if gameinstance[instance].first and gameinstance[instance].draw then gameinstance[instance].draw() end
  --love.graphics.print("Current FPS: "..tostring(love.timer.getFPS( )), 10, 10)
  --local stats = love.graphics.getStats()

  --local str = string.format("Memory used: %.2f MB", stats.texturememory / 1024 / 1024)
  --love.graphics.print(str, 10, 40)
end

function love.textinput(t)
  if gameinstance[instance].textinput then gameinstance[instance].textinput(t) end
end

function love.keypressed(key, scancode, isrepeat)
  if key == "f11" then
    toggle = not toggle
    love.window.setFullscreen(toggle, "desktop")
  end
  if gameinstance[instance].keypressed then gameinstance[instance].keypressed(key, scancode, isrepeat) end
end

function love.mousepressed(x, y, button)
  if gameinstance[instance].mousepressed then gameinstance[instance].mousepressed(x, y, button) end
end
