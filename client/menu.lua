local state = {}
state.first = false
state.instance = nil
local functions = require("openfun-core.functions")
local network = require("openfun-core.network")
local icons = functions.recursiveEnumerate("images/icons")
--TODO fix email in credits
local credits = love.graphics.newImage("credits.png")
local love = love
local suit = require("suit")
local width = 0
local height = 0
local assert = assert

local rows = {}
local slider = {value = 1, min = 1, max = 2,step=1}
local event_timer = {value = 1, min = 1, max = 2,step=1}
local input = {text = ""}
local items = require "items"
local selected = "menu"
local info = nil
local update = false
local search = ""
local zacatek = false
local round = 1
local timer = 5
local scale = 1
local old_scale = 1
local default_font = love.graphics.newFont( 30*scale )
local popup = {
  text = "",
  display = false
}

function send_item(name, value)
  if name == "add_action" then
    local result = network.tcp(name.." "..user.id.." "..value)
    --if result then
      user.action = value
    --end
  end

  if name == "list_day" then
    local tmp_string = ""
    if value == "Today" then
      temp = os.date("*t")
      tmp_string = name.." "..user.id.." "..temp.year.." "
      tmp_string = tmp_string.." "..temp.month.." "..temp.day
    end
    if value == "Yesterday" then
      temp = os.date("*t")
      tmp_string = name.." "..user.id.." "..temp.year.." "
      tmp_string = tmp_string.." "..temp.month.." "..(temp.day-1)
    end
    local result = network.tcp(tmp_string)
    local tmp_popup = ""
    for i,v in pairs(result) do
      tmp_str = v.action..": "..v.duration.."\n"
      tmp_popup = tmp_popup..tmp_str
    end
    if tmp_popup ~= "" then
      popup.text = tmp_popup
      popup.display = true
    end
  end
end

function state.load()
  network.load()
  if not network.test() then
    print("ERROR: Server is not avaible")
    os.exit()
  end
end

local function suit_update(dt)
  suit.layout:reset(10,10)
  suit.layout:padding(10,10)

  suit.Label(items[selected].name,suit.layout:row(width-20,50*scale))
  --suit.Input(input, suit.layout:row(200,50))

  suit.layout:reset(10,20+(50*scale))
  suit.layout:padding(10,10)

  local items_height = math.ceil(height/70)
  if selected then
    rows = items[selected].options
  end
  if #rows > 0 then
    slider.max = #rows
    for  i=functions.round(slider.value),items_height+slider.value,1 do
      if rows[i] then
       --print(rows[i].name)
        if suit["Button"](rows[i], suit.layout:row(width-20,50*scale)).hit then
          if(items[selected].action) then
            send_item(items[selected].action, rows[i])
          else
            selected  = string.lower(rows[i])
          end
        end
      end
   end
     --suit.Slider(slider, {vertical = true},450,70,20,height-90)
  end

  suit.layout:reset(10,height-(70*scale))
  suit.layout:padding(10,10)

  if user.action then
    suit.Label(user.action,suit.layout:row(width-20,50*scale))
  end
end

function state.update(dt)
   width = love.graphics.getWidth( )
   height = love.graphics.getHeight( )
   scale = functions.round((height/960)*10)/10
   if scale ~= old_scale then
     print(scale)
     default_font = love.graphics.newFont( 30*scale )
     love.graphics.setFont(default_font)
     old_scale = scale
   end

   if not popup.display then
     suit_update(dt)
   end

 if timer > 0 then
   timer = timer - dt
 end

end

function state.draw()
  if not popup.display then
    suit.draw()
  else
    love.graphics.printf( popup.text,10,10,width-20,"center")
  end
end

function state.textinput(t)
  suit.textinput(t)
end

function state.keypressed(key)
  if key and popup.display then
    popup.display = false
    selected = "menu"
  end
  if key == "escape" then
    if selected == "menu" then
        state.instance = "quit"
    end
    selected = "menu"
  end
  suit.keypressed(key)
end

function state.mousepressed(x, y, button)
  if button and popup.display then
    popup.display = false
    selected = "menu"
  end
end

return state
