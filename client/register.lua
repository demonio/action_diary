local state = {}
state.player = {}
state.instance = {}
state.first = false
local button = {}
local functions = require "openfun-core.functions"
local status = "OFFLINE"
local timer = 0
local scale = 1
local bg_bg = functions.generateBox(450, 280, {75,75,255,100}, 0)
local bg = functions.generateBox(440, 270, {75,75,255,100}, -75)
local registered = false
local love = love
local tostring = tostring
local string = string
local network = require "openfun-core.network"
local suit = require "suit"
local language = {}
local os = os
state.updater = false
if love.filesystem.isFile("client.language") then
  language = require(love.filesystem.read("client.language"))
else
  language = require("text/english")
end
local username = { text = language.username }
local password = { text = language.password }
local width = 0
local height = 0
local scale = 1
local old_scale = 1

function state.load()
  network.load()
  if love.filesystem.isFile("player.username") then
    username = { text = love.filesystem.read("player.username") }
    password = { text = love.filesystem.read("player.password") }
 end
 if network.test() then
   status = "ONLINE"
 end
 love.keyboard.setKeyRepeat(true)
end

function state.update(dt)
  width = love.graphics.getWidth( )
  height = love.graphics.getHeight( )
  scale = functions.round((height/960)*10)/10
  if scale ~= old_scale then
    print(scale)
    local default_font = love.graphics.newFont( 30*scale )
    bg_bg = functions.generateBox((450*scale), (280*scale), {75,75,255,100}, 0)
    bg = functions.generateBox((440*scale), (270*scale), {75,75,255,100}, -75)
    love.graphics.setFont(default_font)
    old_scale = scale
  end
  suit.Label(language.server_status..status, width/2-(160*scale),height/2-(130*scale), 320*scale,50*scale)
  button.username = suit.Input(username, width/2-(205*scale),height/2-(70*scale), 410*scale,50*scale)
  --button.password = suit.Input(password, width/2-205,height/2-10, 410,50)
  button.login = suit.Button(language.login, width/2-(205*scale),height/2+(50*scale), (200*scale),(50*scale))
  button.register = suit.Button(language.register, width/2+(5*scale),height/2+(50*scale), (200*scale),(50*scale))
  if button.register.hit then
    state.player = {id = os.time(),
                      name=username.text,
                      zivoty=5,
                      level=1,
                      password=password.text}
    if network.tcp("register " .. state.player.name .. " No_password") then
      timer = 300
      registered = true
    end
  end
  if button.login.hit then
    user = network.tcp("login " .. username.text .. " No_password")
    if user then
      --state.player = network.getEntity(username.text)
      if not love.filesystem.isFile("player.username") then
          love.filesystem.newFile("player.username")
          love.filesystem.write("player.username", username.text)
          love.filesystem.newFile("player.password")
          love.filesystem.write("player.password", password.text)
      else
          love.filesystem.write("player.username", username.text)
          love.filesystem.write("player.password", password.text)
      end
      love.keyboard.setKeyRepeat(false)
      state.updater = true
      state.instance = "menu"
    else
      timer = 300
    end
  end
  if timer > 0 then
    if registered then
      suit.Label(language.register_done, width/2-(160*scale),height/2+(135*scale), (320*scale),(50*scale))
    else
      suit.Label(language.login_error, width/2-(160*scale),height/2+(135*scale), (320*scale),(50*scale))
    end
    timer = timer - 1
  end
  if timer == 0 then
    registered = false
  end
end

function state.textinput(t)
  suit.textinput(t)
end

function state.keypressed(key)
  if key == "escape" then
      state.instance = "quit"
  end
  suit.keypressed(key)
end

function state.draw()
  --love.graphics.draw(images.background.grass, 0, 0,0,width/1920, height/1080)
  love.graphics.draw(bg_bg, width/2-(225*scale),height/2-(140*scale))
  love.graphics.draw(bg, width/2-(220*scale),height/2-(135*scale))
  suit.draw()
end

return state
