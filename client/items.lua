return {
  menu = {
    name = "Menu",
    options = {
      "Actions",
      "History"
    }
  },
  actions = {name = "Actions",
  action = "add_action",
  options = {
    "Work",
    "Food",
    "Play",
    "Pub",
    "Excercise",
    "Sleep",
    "Personal_work",
    "Other"
  }},
  history = {name = "History",
  action = "list_day",
  options = {
    "Today",
    "Yesterday"
  }}
}
