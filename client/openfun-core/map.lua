local path = (...):match("(.-)[^%.]+$")
local functions = require(path.."functions")
local map = {}

-- Globals to locals
local require = require
local pairs = pairs
local love = love
local ipairs = ipairs

function map.load(name)
	local tmp = require ('maps/'..name)
	for i,v in pairs(tmp.tilesets) do
		v.imagedata = love.graphics.newImage("maps/"..v.image)
		local tmp_id = v.firstgid
		tmp.tilesets[i].quad = {}
		for y in functions.range(0,v.imageheight-(v.tileheight+v.spacing),v.tileheight+v.spacing) do
			for x in functions.range(0,v.imagewidth-(v.tilewidth+v.spacing),v.tilewidth+v.spacing) do
					tmp.tilesets[i].quad[tmp_id] = love.graphics.newQuad(x, y, v.tilewidth, v.tileheight, v.imagewidth,v.imageheight)
					tmp_id=tmp_id+1
			end
		end
		v.lastgrid = tmp_id
	end

	-- generate map so we can detect better
	for i,v in ipairs(tmp.layers) do
		v.map = {}
		for x in functions.range(1,v.width,1) do
			v.map[x] = {}
			for y in functions.range(1,v.height,1) do
				v.map[x][y] = v.data[x+((y-1)*v.width)]
			end
		end
	end

	return tmp
end

function map.get_id(tmp,what,value,tile_id)
	local id = 0
	local tile_id = tile_id or 1

	local tile_data = tmp.tilesets[tile_id].tiles
	for i,v in ipairs(tile_data) do
		if v.properties[what] == value then
			id = v.id+tmp.tilesets[tile_id].firstgid
		end
	end

	return id
end

function map.find_id(tmp,id,layer)
	local value = nil
	local layer = layer or 1
	local write = io.write

	for x in ipairs(tmp.layers[layer].map) do
		for y in ipairs(tmp.layers[layer].map[x]) do
			if tmp.layers[layer].map[x][y] == id-1 then
				value = {x=x,y=y}
			end
		end
	end

	return value
end

function map.get_checkbox(test,gx,gy)
	local check = {
		left = {
		 	x = functions.round((gx-1)/test.tilewidth),
			y = functions.round((gy/test.tileheight))
		},
		right = {
		 	x = functions.round((gx+(test.tilewidth))/test.tilewidth),
			y = functions.round((gy/test.tileheight))
		},
		down = {
			x = functions.round((gx+(test.tilewidth/2))/test.tilewidth),
			y = functions.round((gy+(test.tileheight))/test.tileheight)
		},
		up = {
		 	x = functions.round((gx+(test.tilewidth/2))/test.tilewidth),
			y = functions.round((gy-1)/test.tileheight)
		},
		center = {
		 	x = functions.round((gx+(test.tileheight/2))/test.tilewidth),
			y = functions.round((gy+(test.tileheight/2))/test.tileheight)
		}
	}
	for key, value in pairs(check) do
		value.id = map.test(value.x,value.y,test,1)
		value.test = map.get_properties(test,value.id)
	end

	return check
end

function map.get_properties(tmp,id)
	local tile_data = tmp.tilesets[1].tiles
	local value = nil
	if id ~= nil then
		for i,v in ipairs(tile_data) do
			if v.id == id-1 then
				value = v
			end
		end
	end
	return value
end

function map.test(x,y,tmp,layer)
	if not tmp.layers[layer].map[x] then return nil end
	return tmp.layers[layer].map[x][y] or nil
end

function map.set(x,y,tmp,layer,value)
	tmp.layers[layer].map[x][y] = value
	local width = tmp.layers[layer].width
	local id = x+((y-1)*width)
	tmp.layers[layer].data[id] = value
end

function map.draw(tmp,tx,ty,scale)
	local mtx = (tx or 0)+love.graphics.getWidth()
	local mty = (ty or 0)+love.graphics.getHeight()
	local ty = ty or 0
	local tx = tx or 0
	local scale = scale or 1
  for i,v in ipairs(tmp.layers) do
		if v.type == "tilelayer" then
			for k,z in pairs(tmp.tilesets) do
					local tmp_id = 1
					local y_min = functions.round(ty/(z.tileheight*scale))
					y_min = (y_min > 1 and y_min-1 or 1)
					local y_max = functions.round(mty/(z.tileheight*scale))
					y_max = (y_max < v.height and y_max+1 or v.height)
					for y in functions.range(y_min,y_max,1) do
						local x_min = functions.round(tx/(z.tilewidth*scale))
						x_min = (x_min > 1 and x_min-1 or 1)
						local x_max = functions.round(mtx/(z.tilewidth*scale))
						x_max = (x_max < v.width and x_max+1 or v.width)
						for x in functions.range(x_min,x_max,1) do
							tmp_id = x+((y-1)*v.width)
							--print(x_min,x_max,y_min,y_max,tmp_id)
							if (z.firstgid <= v.data[tmp_id] or v.data[tmp_id] == 0) and z.lastgrid > v.data[tmp_id] then
								if v.data[tmp_id] > 0 then
									love.graphics.draw(z.imagedata,
		 						 				tmp.tilesets[k].quad[v.data[tmp_id]],
		 						 				(((x-1)*z.tilewidth)*scale)-tx,
		 										(((y-1)*z.tileheight)*scale)-ty,
		 										0, scale, scale,
		 										v.offsetx,
		 										v.offsety)
								end

							end
						end
				end
			end
		end
	end
end

function map.ldraw(tmp,tx,ty,scale,layer)
	local mtx = (tx or 0)+love.graphics.getWidth()
	local mty = (ty or 0)+love.graphics.getHeight()
	local ty = ty or 0
	local tx = tx or 0
	local scale = scale or 1
	local layer = layer or 1
		local v = tmp.layers[layer]
		if v.type == "tilelayer" then
			for k,z in pairs(tmp.tilesets) do
					local tmp_id = 1
					local y_min = functions.round(ty/(z.tileheight*scale))
					y_min = (y_min > 1 and y_min-1 or 1)
					local y_max = functions.round(mty/(z.tileheight*scale))
					y_max = (y_max < v.height and y_max+1 or v.height)
					for y in functions.range(y_min,y_max,1) do
						local x_min = functions.round(tx/(z.tilewidth*scale))
						x_min = (x_min > 1 and x_min-1 or 1)
						local x_max = functions.round(mtx/(z.tilewidth*scale))
						x_max = (x_max < v.width and x_max+1 or v.width)
						for x in functions.range(x_min,x_max,1) do
							tmp_id = x+((y-1)*v.width)
							--print(x_min,x_max,y_min,y_max,tmp_id)
							if (z.firstgid <= v.data[tmp_id] or v.data[tmp_id] == 0) and z.lastgrid > v.data[tmp_id] then
								if v.data[tmp_id] > 0 then
									love.graphics.draw(z.imagedata,
		 						 				tmp.tilesets[k].quad[v.data[tmp_id]],
		 						 				(((x-1)*z.tilewidth)*scale)-tx,
		 										(((y-1)*z.tileheight)*scale)-ty,
		 										0, scale, scale,
		 										v.offsetx,
		 										v.offsety)
								end

							end
						end
				end
			end
		end
end

return map
